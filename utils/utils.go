package utils

import (
	"bytes"
	"encoding/json"
	"io"
	"time"

	"gitlab.com/brurberg/log/v2"
)

type PackageConsts struct {
	Domain string `json:"domain"`
	Key    string `json:"key"`
}

type LogPackage struct {
	Domain    string    `json:"domain"`
	Key       string    `json:"key"`
	Data      log.Log   `json:"data"`
	ErrorTime time.Time `json:"errortime"`
}

func (pc PackageConsts) LogToPackage(l log.Log) (io.Reader, error) {
	buf := new(bytes.Buffer)
	err := json.NewEncoder(buf).Encode(LogPackage{Domain: pc.Domain, Key: pc.Key, Data: l, ErrorTime: time.Now()})
	return buf, err
}
