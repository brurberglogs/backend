package helper

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type JwtToken struct {
	Token string `json:"token"`
}

var WeakSystemSecret = fmt.Errorf("Not strong enught system secret")

func SignJwt(claims jwt.MapClaims, secret []byte) (string, error) {
	if len(secret) < 32 { //TODO: remove static lenth of secret string.
		return "", WeakSystemSecret
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}

func VerifyJwt(token string, secret []byte) (map[string]interface{}, error) {
	jwToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error signing the token")
		}
		return secret, nil
	})
	if err != nil {
		return nil, err
	}
	if !jwToken.Valid {
		return nil, fmt.Errorf("Invalid authorization token")
	}
	return jwToken.Claims.(jwt.MapClaims), nil
}

func GetBearerToken(header string) (string, error) {
	if header == "" {
		return "", fmt.Errorf("An authorization header is required")
	}
	token := strings.Split(header, " ")
	if len(token) != 2 {
		return "", fmt.Errorf("Malformed bearer token")
	}
	return token[1], nil
}

func GetClaims(token string) (claims map[string]interface{}) {
	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return
	}
	dec, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(json.Unmarshal(dec, &claims))
	return
}

func GetJWTSecret() []byte {
	return []byte(os.Getenv("JWT_SECRET"))
}
