package helper

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"

	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurberglogs/backend/utils"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBConnect) ConnectToDB() (DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return DB{}, err
	}

	return DB{db}, db.Ping()
}

func (db DB) NewEntry(packet utils.LogPackage) (err error) {
	result, err := db.Conn.Exec("INSERT INTO entries (domain, level, context, message, errorTime) VALUES ($1, $2, $3, $4, $5);", packet.Domain, packet.Data.Level, packet.Data.Context, packet.Data.Message, packet.ErrorTime)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

type LogEntrie struct {
	Domain    string    `json:"domain"`
	level     log.Level `json:"~"`
	Level     string    `json:"level"`
	Context   string    `json:"context"`
	Message   string    `json:"message"`
	ErrorTime time.Time `json:"error_time"`
}

func (db DB) GetAll() (logEntries []LogEntrie, err error) {
	rows, err := db.Conn.Query("SELECT domain, level, context, message, errorTime FROM entries;")
	if err != nil {
		return
	}
	for rows.Next() {
		var logEntrie LogEntrie
		if log.CheckError("Log Entrie Fetching", rows.Scan(&logEntrie.Domain, &logEntrie.level, &logEntrie.Context, &logEntrie.Message, &logEntrie.ErrorTime), log.Warning) {
			continue
		}
		logEntrie.Level = log.LevelToString(logEntrie.level)
		logEntries = append(logEntries, logEntrie)
	}
	return
}

func (db DB) IsAdmin(username string) (isadmin bool) {
	/*
		err := db.Conn.QueryRow("SELECT IsAdmin FROM users WHERE username = $1;", username).Scan(&isadmin)
		if err != nil {
			return false
		}
		return
	*/
	return username == "1231" || username == "eskil09"
}
