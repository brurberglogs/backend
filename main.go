package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurberglogs/backend/helper"
	"gitlab.com/brurberglogs/backend/utils"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

var db helper.DB

func main() {
	loadEnvVar()

	r := gin.Default()
	var dbErr error
	db, dbErr = helper.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}

	api := r.Group("/api")
	api.POST("/newentry", func(c *gin.Context) {
		var entry utils.LogPackage
		c.BindJSON(&entry)
		log.Println(entry)
		err := db.NewEntry(entry)
		if err != nil {
			log.Println(err)
		}
	})

	auth := api.Group("/")
	auth.Use(AuthMiddleware())

	admin := auth.Group("/")
	admin.Use(AdminMiddleware())
	admin.GET("/all", func(c *gin.Context) {
		entries, err := db.GetAll()
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{"error": err.Error()})
		}
		c.JSON(200, entries)
	})

	r.Run(":3000")
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		handleErr := func(status int) func(error) {
			return func(err error) {
				c.JSON(status, gin.H{"error": err.Error()})
				c.Abort()
			}
		}
		bearerToken, err := helper.GetBearerToken(c.Request.Header.Get("authorization"))
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			handleErr(http.StatusBadRequest)
			return
		}
		claims := helper.GetClaims(bearerToken)
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["claims"] = claims
		if h, ok := claims["host"]; !ok || c.Request.Host != h {
			c.JSON(http.StatusForbidden, gin.H{"error": "Missmatch in hosts"})
			c.Abort()
			return
		}
		_, err = helper.VerifyJwt(bearerToken, helper.GetJWTSecret())
		if log.CheckError("AuthMiddleware", err, log.Warning) {
			handleErr(http.StatusForbidden)
			return
		}
		c.Next()
	}
}

func AdminMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		claims := c.Keys["claims"].(map[string]interface{})
		if uname, ok := claims["username"]; ok {
			if !db.IsAdmin(uname.(string)) {
				c.JSON(http.StatusForbidden, gin.H{"error": "Admin privelages is needed"})
				c.Abort()
				return
			}
		} else {
			c.JSON(http.StatusForbidden, gin.H{"error": "Username was not provided"})
			c.Abort()
			return
		}
		c.Next()
	}
}
